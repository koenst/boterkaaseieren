﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boterkaaseieren.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public BKESymbol XO { get; set; }
        public Player(int id, BKESymbol xo)
        {
            Id = id;
            XO = xo;
            Points = 0;
        }
    }
    public enum BKESymbol
    {
        X,
        O
    }
}
