﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Boterkaaseieren.Models;

namespace Boterkaaseieren
{
    public partial class MainWindow : Window
    {
        private Player Player1 { get; set; }
        private Player Player2 { get; set; }
        private Player currentPlayer;
        private List<Button> Buttons;
        private int Turn = 0;
        private int StartPlayerID;
        private bool GameHasEnded;
        public MainWindow()
        {
            InitializeComponent();
            this.Width = 500;
            this.Height = 500;
            this.Title = "Ontbijt!";

            if (GetButtons())
            {
                foreach (Button b in Buttons)
                {
                    panel1.Children.Add(b);
                }
                SetPlayers();
            }

        }

        private void SetPlayers()
        {
            Player1 = new Player(1, BKESymbol.X);
            Player2 = new Player(2, BKESymbol.O);
            currentPlayer = Player1;
            StartPlayerID = currentPlayer.Id;
        }

        bool GetButtons()
        {
            Buttons = new List<Button>();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var b = new Button();
                    var x = "";
                    var y = "";
                    switch (i)
                    {
                        case 0:
                            x = "L";
                            break;
                        case 1:
                            x = "M";
                            break;
                        case 2:
                            x = "R";
                            break;
                        default:
                            x = "X";
                            break;
                    }
                    switch (j)
                    {
                        case 0:
                            y = "T";
                            break;
                        case 1:
                            y = "M";
                            break;
                        case 2:
                            y = "B";
                            break;
                        default:
                            y = "Y";
                            break;
                    }
                    b.Name = string.Format("btn{0}{1}", x, y);
                    b.Click += Btn_Click;
                    b.Height = 75;
                    b.Width = 75;
                    Buttons.Add(b);
                }
            }
            return true;
        }

        public Player GetPlayer(int id)
        {
            return id == Player1.Id ? Player1 : Player2;
        }

        void SetCurrentPlayer()
        {
            currentPlayer = Turn % 2 == 0 ? GetPlayer(StartPlayerID) : GetSecondPlayer();
            lbCurrent.Content = string.Format("{0} is aan de beurt!", currentPlayer.Name);
        }

        private Player GetSecondPlayer()
        {
            if (Player1 == null || Player2 == null)
                return null;
            return StartPlayerID == Player1.Id ? Player2 : Player1;
        }

        void Btn_Click(object sender, RoutedEventArgs e)
        {
            Button Sender = (Button)sender;

            if (!GameHasEnded)
            {
                if (Sender.Content == null)
                {
                    Turn += 1;
                    Sender.Content = currentPlayer.XO;
                    if (WinningMove(Sender))
                    {
                        GameHasEnded = true;
                        AddPoints();
                        btnNew.Visibility = Visibility.Visible;
                    }
                    else if (Turn >= 9)//Veld is vol, remise. 
                    {
                        SetRemise();
                    }
                    else
                    {
                        SetCurrentPlayer();
                    }
                }
                else //Kies een ander veld, deze is al gebruikt. 
                {

                }
            }
        }

        private void SetRemise()
        {
            GameHasEnded = true;
            lbCurrent.Content = "Gelijkspel! ";
            btnNew.Visibility = Visibility.Visible;
        }

        private void AddPoints()
        {
            lbCurrent.Content = string.Format("Hoera, {0} heeft gewonnen!", currentPlayer.Name);
            currentPlayer.Points += 1;
            if (Player1.Points > 0)
            {
                lbPoints1.Content = string.Format("Punten: {0}", Player1.Points);
            }
            if (Player2.Points > 0)
            {
                lbPoints2.Content = string.Format("Punten: {0}", Player2.Points);
            }
        }

        private void NewGame()
        {
            Turn = 0;
            ClearButtons();
            ChangeStartPlayer();
            SetCurrentPlayer();
            GameHasEnded = false;
        }

        private void ChangeStartPlayer()
        {
            if(Player1 != null && Player2 != null)
                StartPlayerID = StartPlayerID == Player1.Id ? Player2.Id : Player1.Id;
        }

        bool WinningMove(Button sender)
        {
            //Check for horizontal row:
            if (Buttons.Where(b =>
            b.Content != null &&
            b.Name[3] == sender.Name[3] &&
            ((BKESymbol)b.Content == currentPlayer.XO)).Count() == 3)
            {
                return true;
            }
            //Check for vertical row:
            else if (Buttons.Where(b =>
            b.Content != null &&
            b.Name[4] == sender.Name[4] &&
            ((BKESymbol)b.Content == currentPlayer.XO)).Count() == 3)
            {
                return true;
            }
            //Check for diagonal row:
            var bMM = Buttons.Where(b => b.Name == "btnMM").FirstOrDefault();
            var bLT = Buttons.Where(b => b.Name == "btnLT").FirstOrDefault();
            var bRB = Buttons.Where(b => b.Name == "btnRB").FirstOrDefault();
            var bRT = Buttons.Where(b => b.Name == "btnRT").FirstOrDefault();
            var bLB = Buttons.Where(b => b.Name == "btnLB").FirstOrDefault();
            if (bMM.Content != null &&
                (BKESymbol)bMM.Content == currentPlayer.XO)//Middle button is marked by current player
            {
                if ((bLT.Content != null &&
                    (BKESymbol)bLT.Content == currentPlayer.XO &&
                    bRB.Content != null &&
                    (BKESymbol)bRB.Content == currentPlayer.XO) ||//Either left top && right bottom or right top and left bottom are marked by current player, forming a diagonal line with the middle button. 
                    (bRT.Content != null &&
                    (BKESymbol)bRT.Content == currentPlayer.XO &&
                    bLB.Content != null &&
                    (BKESymbol)bLB.Content == currentPlayer.XO))
                {
                    return true;
                }
            }
            return false;
        }

        private void ClearButtons()
        {
            foreach (Button b in Buttons)
            {
                b.Content = null;
            }
        }

        private void TbName1_TextChanged(object sender, TextChangedEventArgs e)
        {
            var Sender = (TextBox)sender;
            if (Player1 != null)
            {
                Player1.Name = Sender.Text;
                if (currentPlayer.Id == Player1.Id)
                {
                    lbCurrent.Content = string.Format("{0} is aan de beurt!", currentPlayer.Name);
                }
            }
        }
        private void TbName2_TextChanged(object sender, TextChangedEventArgs e)
        {
            var Sender = (TextBox)sender;
            if (Player2 != null)
            {
                Player2.Name = Sender.Text;
                if (currentPlayer.Id == Player2.Id)
                {
                    lbCurrent.Content = string.Format("{0} is aan de beurt!", currentPlayer.Name);
                }
            }
        }

        private void BtnNewClick(object sender, RoutedEventArgs e)
        {
            btnNew.Visibility = Visibility.Hidden;
            NewGame();
        }
    }
}